data:extend({
	{
		type = "string-setting",
		name = "dirty_talk",
		setting_type = "runtime-global",
		default_value = "all",
		allowed_values = {"all", "only chat", "only player"}
	}
})
