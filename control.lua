local dirty_talk = require("dirty_talk/control")

script.on_event(defines.events.on_console_chat, dirty_talk.events.on_console_chat)
script.on_event(defines.events.on_console_command, dirty_talk.events.on_console_command)
script.on_nth_tick(90, dirty_talk.player_check)
