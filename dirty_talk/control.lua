--[[
Copyright (c) 2018-2019 ZwerOxotnik <zweroxotnik@gmail.com>
Licensed under the MIT licence;
Author: ZwerOxotnik
Version: 1.2.0 (2019.01.07)

You can write and receive any information on the links below.
Source: https://gitlab.com/ZwerOxotnik/dirty-talk
Mod portal: https://mods.factorio.com/mod/dirty-talk
Homepage: https://forums.factorio.com/viewtopic.php?f=190&t=64624

]]--

local module = {}

local unnecessary_commands = {
    w = true,
    whisper = true,
    s = true,
    shout = true
}

module.player_check = function()
    local setting = settings.global["dirty_talk"].value
    if setting ~= "all" and setting ~= "only player" then return end

    for _, player in pairs(game.connected_players) do
        if player.valid and player.character then
            local character = player.character
            character.surface.pollute(character.position, 8)
        end
    end
end

local function on_console_chat(event)
    local setting = settings.global["dirty_talk"].value
    if setting ~= "all" and setting ~= "only chat" then return end
    local player = game.players[event.player_index]
    if not(player and player.valid and player.character) then return end

    local character = player.character
    character.surface.pollute(character.position, string.len(event.message))
end

local function on_console_command(event)
    local setting = settings.global["dirty_talk"].value
    if setting ~= "all" and setting ~= "only chat" then return end
    local player = game.players[event.player_index]
    if not(player and player.valid and player.character) then return end

    if unnecessary_commands[event.command] then
        local character = player.character
        character.surface.pollute(character.position, string.len(event.parameters))
    end
end

module.events = {
    ["on_console_command"] = on_console_command,
    ["on_console_chat"] = on_console_chat
}

return module
